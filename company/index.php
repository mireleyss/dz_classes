<?php

    class Employee {

        private string $name;
        private string $email;
        private string $password;
        private Department $department;
        private Role $role;

        public function __construct(string $name, string $email, string $password, Department $department, Role $role) {
            $this->name = $name;
            $this->email = $email;
            $this->password = $password;
            $this->department = $department;
            $this->role = $role;
        }

        public function getName() {
            return $this->name;
        }

        public function getEmail() {
            return $this->email;
        }

        public function getPassword() {
            return $this->password;
        }
        
        public function getDepartment() {
            return $this->department;
        }

        public function getRole() {
            return $this->role;
        }

    }



    class Department {

        private string $name;

        public function __construct(string $name) {
            $this->name = $name;
        }

        public function getName() {
            return $this->name;
        }
    }



    class Permission {

        private int $id;
        private string $name;

        public function __construct(int $id, string $name) {
            $this->id = $id;
            $this->name = $name;
        }

        public function getId() {
            return $this->id;
        }

        public function getName() {
            return $this->name;
        }
    }



    class Role {
        private string $name;
        private array $permissions;

        public function __construct(string $name, array $permissions) {
            $this->name = $name;
            $this->permissions = $permissions;
        }

        public function getName() {
            return $this->name;
        }

        public function getPermissions() {
            return $this->permissions;
        }

        public function addPermission(Permission $permission) {
            return $this->permissions[] = $permission;
        }

        public function removePermission(Permission $permission) {
            if (array_key_exists($key = (array_search($permission, $this->permissions)), $this->permissions)){
                unset($this->permissions[$key]);
            }
            
        }
    }



    class AccessControl {

        private array $roles;
        private array $permissions;

        public function __construct(array $roles, array $permissions) {
            $this->roles = $roles;
            $this->permissions = $permissions;
        }

        public function getRoles() {
            return $this->roles;
        }

        public function getPermissions() {
            return $this->permissions;
        }

        public function checkAccess(Employee $employee, Permission $permission) {
            if(!empty($employee->getRole()->getPermissions())) {
                foreach($employee->getRole()->getPermissions() as $employeePermission) {
                    if ($employeePermission->getId() == $permission->getId() && $employeePermission->getName() == $permission->getName()) {
                        echo $employee->getName() . ' имеет доступ!';
                        return true;
                    } else {
                        echo 'В доступе отказано';
                        return false;
                    }
                }
            } else {
                echo 'У вас нет никаких прав доступа';
            }
            
        }
    }

    $departmentManagement = new Department('Sales');
    $departmentAccounting = new Department('Accounting');
    $departmentHR = new Department('HR');
    $departmentTraining = new Department('Training');

    $permission1 = new Permission(100, 'Add employees');
    $permission2 = new Permission(101, 'Remove employees');
    $permission3 = new Permission(102, 'Add report');
    $permission4 = new Permission(103, 'Remove report');
    $permission5 = new Permission(104, 'Read private documents');
    $permission6 = new Permission(105, 'Access to sales data');

    $role1 = new Role('CEO', [$permission1, $permission2, $permission4, $permission5, $permission6]);
    $role2 = new Role('Accountant', [$permission3, $permission4, $permission6]);
    $role3 = new Role('Trainee', []);
    $role4 = new Role('HR', [$permission1, $permission2]);

    $role4->addPermission($permission5);
    print_r($role4->getPermissions());
    echo '<br>';
    echo '<br>';
    $role4->removePermission($permission1);
    print_r($role4->getPermissions());
    echo '<br>';

    $employee1 = new Employee('Albert', 'albert@gmail.com', 'albert1957', $departmentTraining, $role3);
    $employee2 = new Employee('Frederic', 'frederic@gmail.com', 'frederic2001', $departmentHR, $role4);
    $employee3 = new Employee('Klaudine', 'klaudine@gmail.com', 'klaudine1986', $departmentManagement, $role1);
    $employee4 = new Employee('Meghan', 'meghan@gmail.com', 'Meghan454567', $departmentAccounting, $role2);

    $checkAccess1 = new AccessControl([$role3], [$permission3]);
    echo $checkAccess1->checkAccess($employee1, $permission4) . '<br>';
    echo $checkAccess1->checkAccess($employee2, $permission1) . '<br>';
    echo $checkAccess1->checkAccess($employee3, $permission3) . '<br>';