<?php 

    class Vehicle {
        private string $brand;
        private string $model;
        private int $maxSpeed;
        private int $wheelsCount;
        private int $releaseYear;

        public function __construct(string $brand, string $model, int $maxSpeed, int $wheelsCount, int $releaseYear) {
            $this->brand = $brand;
            $this->model = $model;
            $this->maxSpeed = $maxSpeed;
            $this->wheelsCount = $wheelsCount;
            $this->releaseYear = $releaseYear;
        }

        public function getBrand() {
            return $this->brand;
        }

        public function getModel() {
            return $this->model;
        }

        public function getMaxSpeed() {
            return $this->maxSpeed;
        }

        public function getYear() {
            return $this->releaseYear;
        }

        public function getWheelsCount() {
            return $this->wheelsCount;
        }

        public function getInfo() {
            echo 'Транспорт бренда ' . $this->brand . ', модель ' . $this->model . ', максимальная скорость ' . $this->maxSpeed . ', количество колес ' . $this->wheelsCount . ', год ' . $this->releaseYear;
        }
    }

    class Car extends Vehicle {
        private int $doorsCount;
        private float $engineSize;
        private int $accelerationTo100;
        private bool $isStarted;

        public function __construct(string $brand, string $model, int $maxSpeed, int $wheelsCount, int $releaseYear, int $doorsCount, float $engineSize, int $accelerationTo100, bool $isStarted) {
            parent::__construct($brand, $model, $maxSpeed, $wheelsCount, $releaseYear);
            $this->doorsCount = $doorsCount;
            $this->engineSize = $engineSize;
            $this->accelerationTo100 = $accelerationTo100;
            $this->isStarted = $isStarted;
        }

        public function inspection() {
            if (date('Y') - $this->getYear() <= 10) {
                echo 'Вам нужно проходить техосмотр раз в 2 года';
            } else {
                echo 'Вам нужно проходить техосмотр раз в год';
            }
        }

        public function startEngine() {
            if ($this->isStarted == false) {
                echo 'Завелись';
                $this->isStarted = true;
            } else {
                echo 'Машина уже заведена';
            }
        }

        public function stopEngine() {
            if ($this->isStarted == true) {
                echo 'Заглушились';
                $this->isStarted = false;
            } else {
                echo 'Машину уже заглушили';
            }
        }

        public function getInfo() {
            echo 'Машина ' . $this->getBrand() . ' ' . $this->getModel() 
            . ', максимальная скорость ' . $this->getMaxSpeed() 
            . ', количество дверей ' . $this->doorsCount 
            . ', объем двигателя ' . $this->engineSize
            . ', разгон до 100 за ' . $this->accelerationTo100 . ' сек.';
        }


    }



    class Motorcycle extends Vehicle {
        private string $type;
        private string $category;

        public function __construct(string $brand, string $model, int $maxSpeed, int $wheelsCount, int $releaseYear, string $type, string $category) {
            parent::__construct($brand, $model, $maxSpeed, $wheelsCount, $releaseYear);
            $this->type = $type;
            $this->category = $category;
        }

        public function backWheel() {
            if ($this->type == 'Motocross' || $this->type == 'Sport') {
                echo 'Вы можете встать на заднее колесо, поздравляем!';
            } else {
                echo 'У вас не получится встать на заднее колесо :(';
            }
        }

        public function getVariation() {
            if ($this->getWheelsCount() == 2) {
                echo 'У вас обычный мотоцикл!';
            } elseif ($this->getWheelsCount() == 3) {
                echo 'У вас трицикл!';
            } elseif ($this->getWheelsCount() == 4) {
                echo 'У вас квадроцикл!';
            }
        }

        public function getCategory() {
            return $this->category;
        }

        public function goOffRoad() {
            if ($this->type == 'Motocross' || $this->type == 'Sport' || $this->type == 'Pit bike') {
                echo 'Вы можете выехать на бездорожье';
            } else {
                echo 'Проезд на бездорожье запрещен';
            }
        }
    }



    class Bicycle extends Vehicle {
        private string $brakeType;
        private string $ageCategory;

        public function __construct(string $brand, string $model, int $maxSpeed, int $wheelsCount, int $releaseYear, string $brakeType, string $ageCategory) {
            parent::__construct($brand, $model, $maxSpeed, $wheelsCount, $releaseYear);
            $this->brakeType = $brakeType;
            $this->ageCategory = $ageCategory;
        }

        public function getBrakeType() {
            return $this->brakeType;
        }

        public function checkAgeCategory() {
            if ($this->ageCategory == 'child') {
                echo 'Ваш велосипед предназначен для детей';
            } else {
                echo 'Ваш велосипед предназначен для взрослых';
            }
        }

    }



    $vehicle = new Vehicle('Mercedes', 'Benz', 250, 4, 2007);
    $vehicle->getInfo();
    $car = new Car('Audi', 'A8', 300, 4, 2020, 4, 5.0, 6, false);
    $car->inspection();
    echo '<br>';
    $car->startEngine();
    $car->stopEngine();
    echo '<br>';
    $motorcycle = new Motorcycle('Yamaha', '1500', 300, 3, 2006, 'Cruiser', 'Heavy');
    $motorcycle->goOffRoad();
    $motorcycle->backWheel();
    $motorcycle->getVariation();
    $motorcycle->getCategory();
    $bicycle = new Bicycle('Aist', '120', 100, 2, 1999, 'Disc', 'child');
    $bicycle->checkAgeCategory();
    $bicycle->getBrakeType();